package com.demo.deployment.service;

import org.apache.maven.shared.invoker.*;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;

@Service
public class MavenService {

    public boolean buildJar(String folderPath) {
        InvocationRequest request = new DefaultInvocationRequest();
       // request.setPomFile(new File(folderPath+"\\pom.xml"));
        request.setPomFile(new File(folderPath+"/pom.xml"));
        request.setGoals(Arrays.asList( "clean", "install" ));

        Invoker invoker = new DefaultInvoker();
        invoker.setMavenHome(new File("C:\\Users\\sosagar\\Downloads\\apache-maven-3.8.3-bin\\apache-maven-3.8.3"));
        try {
            invoker.execute(request);
        } catch (MavenInvocationException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}

package com.demo.deployment.service;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class FileService {

    public File createDirectory(String directoryPath){
        File directory = new File(directoryPath);
        if (! directory.exists()){
            directory.mkdir();
            // If you require it to make the entire directory path including parents,
            // use directory.mkdirs(); here instead.
            return directory;
        }
        return directory;
    }

    public boolean copyFile(String sourceFileName,String destFileName){
        File sourceFile = new File(sourceFileName);
        File destFile = new File(destFileName);

        try {
            org.apache.commons.io.FileUtils.copyFile(sourceFile,destFile);
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}

package com.demo.deployment.controller;

import com.demo.deployment.model.Data;
import com.demo.deployment.service.DataService;
import com.demo.deployment.service.FileService;
import com.demo.deployment.service.GitService;
import com.demo.deployment.service.MavenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/data")
public class DataController {
    @Autowired
    private DataService dataService;
    @Autowired
    private GitService gitService;
    @Autowired
    private MavenService mavenService;
    @Autowired
    private FileService fileService;

    Logger logger=LoggerFactory.getLogger(DataController.class);

    @PostMapping
    public ResponseEntity<String> process(@RequestBody Data data) throws IOException {

        boolean saveDataToNewFile=true;
        File createdTxtFile = null;

        if(!dataService.isValidData(data)) {
            logger.info("data validation failed");
           return new ResponseEntity<>("data validation failed", HttpStatus.OK);
        }

        if(saveDataToNewFile){
            createdTxtFile=dataService.saveDataToNewFile(data);
            if(createdTxtFile==null || createdTxtFile.exists()==false){
                logger.info("Error creating text file");
                return new ResponseEntity<>("Error creating text file", HttpStatus.OK);
            }
        }else{
            dataService.saveDataToExistingFile(data);
        }

        String clonedRepoPath=gitService.cloneOrPullRepo();
        File createdDirectory=fileService.createDirectory("/cloned-project/jar-and-text");


        if(!createdDirectory.exists()){
            logger.info("process is unsuccessful  because cannot create directory or copy the file");
            return new ResponseEntity<>("process is unsuccessful  because cannot create directory or copy the file", HttpStatus.OK);
        }

        if(!mavenService.buildJar(clonedRepoPath)){
            logger.info("process is unsuccessful  because unable to buildJar");
            return new ResponseEntity<>("process is unsuccessful  because unable to buildJar", HttpStatus.OK);
        }

        boolean jarCopied= dataService.copyJarFile();
        assert createdTxtFile != null;
        boolean txtFileCopied=dataService.copyTxtFile(createdTxtFile);

        if(jarCopied && txtFileCopied){
            gitService.commitToRepository();
            logger.info("process successful");
            return new ResponseEntity<>("process successful", HttpStatus.OK);
        }else{
            logger.info("process is unsuccessful  because jar or txt file is not copied");
            return new ResponseEntity<>("process is unsuccessful  because jar or txt file is not copied", HttpStatus.OK);
        }


    }


}

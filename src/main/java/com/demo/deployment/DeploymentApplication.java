package com.demo.deployment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class DeploymentApplication {

	public static void main(String[] args){
	//	System.out.println(System.getenv("M2_HOME"));
		SpringApplication.run(DeploymentApplication.class, args);

	}

}

package com.demo.deployment.model;


public class Data {

    private String companyName;
    private String emailId;
    private String phoneNumber;
    private String country;
    private String city;
    private String street;
    private String zipCode;
    private String language;
    private String logoImageUpload;

    public Data(String companyName, String emailId, String phoneNumber, String country, String city, String street, String zipCode, String language, String logoImageUpload) {
        this.companyName = companyName;
        this.emailId = emailId;
        this.phoneNumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.street = street;
        this.zipCode = zipCode;
        this.language = language;
        this.logoImageUpload = logoImageUpload;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getLogoImageUpload() {
        return logoImageUpload;
    }

    public void setLogoImageUpload(String logoImageUpload) {
        this.logoImageUpload = logoImageUpload;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "companyName = " + companyName  + "\n" +
                "emailId = " + emailId  +"\n" +
                "phoneNumber = " + phoneNumber  +"\n" +
                "country = " + country +"\n" +
                "city = " + city +"\n" +
                "street = " + street +"\n" +
                "zipCode = " + zipCode +"\n" +
                "language = " + language + "\n";
    }
}

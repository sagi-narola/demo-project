package com.demo.deployment.service;

import com.demo.deployment.model.Data;
import org.apache.maven.shared.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class DataService {

    @Autowired
    FileService fileService;

    public boolean isValidData(Data data){

        if(StringUtils.isBlank(data.getCompanyName()) || StringUtils.isBlank(data.getEmailId()) || StringUtils.isBlank(data.getPhoneNumber()) || StringUtils.isBlank(data.getCountry())){
            return false;
        }
        return true;
    }

   public File saveDataToNewFile(Data data) throws IOException {
        String directoryName="text-files";
        File directory=fileService.createDirectory(directoryName);
        String fileName=directoryName+"/"+new SimpleDateFormat("ddMMyyyyHHmm'.txt'").format(new Date());
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(data.toString());
        writer.close();
       return new File(fileName);
    }

    public void saveDataToExistingFile(Data data) throws IOException {
        String directoryName="text-files";
        File directory=fileService.createDirectory(directoryName);
        String fileName=directoryName+"/"+new SimpleDateFormat("ddMMyyyyHHmm'.txt'").format(new Date());
        FileWriter fw = new FileWriter("E:\\sample.txt",true);
        PrintWriter out = new PrintWriter(fw);
        out.println(data.toString());
        out.close();
    }

    public boolean copyJarFile(){
        String jarSource = "cloned-project/target/deployment-0.0.1-SNAPSHOT.jar";
        String destFolder = "cloned-project/jar-and-text/";
        String destJarFileName ="deployment-0.0.1-SNAPSHOT-"+ new SimpleDateFormat("ddMMyyyyHHmm'.jar'").format(new Date());
        String jarDest=destFolder+destJarFileName;
        return fileService.copyFile(jarSource,jarDest);
    }

    public boolean copyTxtFile(File createdTxtFile){
        String txtSource=createdTxtFile.getAbsolutePath();
        String destFolder = "cloned-project/jar-and-text/";
        String txtDest=destFolder+createdTxtFile.getName();
        return fileService.copyFile(txtSource,txtDest);
    }

}
